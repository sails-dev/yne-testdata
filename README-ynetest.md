# YNE Test Data

This repository contains data used by the test suite for YNE.

All files in this repository except those explicitly listed below are licensed
under the Creative-Commons 3.0 Attribution license.  To view a copy of this
license see the `CC-3.0-BY.txt` file.
